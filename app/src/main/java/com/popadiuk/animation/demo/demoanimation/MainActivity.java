package com.popadiuk.animation.demo.demoanimation;

import android.app.Activity;
import android.app.SearchManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.popadiuk.animation.demo.utils.GifAnimateView;


public class MainActivity extends Activity {
    private Button anim1;
    private Button anim2;
    private Button anim3;
    private View.OnClickListener mOnClickListener;
    private GifAnimateView gifBackground;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initview();
        gifBackground=(GifAnimateView)findViewById(R.id.gifBackground);
        gifBackground.setIsBackground(true);
        gifBackground.setMovieResource(R.drawable.test);
    }
    private void initview(){

 mOnClickListener= new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.anim1:
                        gifBackground.setMovieResource(R.drawable.anim01);
                        break;
                    case R.id.anim2:
                        gifBackground.setMovieResource(R.drawable.minute);
                        break;
                    case R.id.anim3:
                        gifBackground.setMovieResource(R.drawable.test);
                        break;
                }
            }
        };
        anim1 = (Button) findViewById(R.id.anim1);
        anim1.setOnClickListener(mOnClickListener);
        anim2 = (Button) findViewById(R.id.anim2);
        anim2.setOnClickListener(mOnClickListener);
        anim3 = (Button) findViewById(R.id.anim3);
        anim3.setOnClickListener(mOnClickListener);

    }

}
